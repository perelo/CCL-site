---
title: Com & Sea
header:
    image: assets/images/logo-com-grand.png
---

Le Com&Sea est une association rassemblant les étudiants en Océanographie de
Marseille (les étudiants de la licence SVT parcours Mer mais également ceux du
master d'océanographie). Rattachée à l’Institut Océanologique de Marseille
(M.I.O.), à l’Institut Pythéas et à la faculté des Sciences de Luminy, elle a
pour objectif de dynamiser la vie étudiante et de créer un réseau mettant en
relation les étudiants actuels, les anciens étudiants et les professionnels du
monde de la mer.

Dans ce rôle de passerelle, l’association propose à ses adhérents des prix
réduits pour pratiquer des activités marines telles que la plongée sous-marine,
ou encore pour passer son permis bateau.

L’association Com and Sea a pour but d’organiser des rencontres entre étudiants
en Océanographie par le biais de soirées étudiantes, week-ends, séjours et
autres événements, mais aussi de sensibiliser les étudiants en organisant des
projections et conférences autour des thèmes de l’environnement.

*Site Luminy*
