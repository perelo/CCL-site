---
title: Adhésion et soutiens
toc: true
share: true
header:
  overlay_image: assets/images/banniere-bulles.png
  og_image: assets/images/logo-ccl-grand.png
---

# Adhérez à l'asso du CCL

**C'est 1€ symbolique ;)** Comment adhérer ?
- En ligne sur [<i class="fas fa-fw fa-fire" aria-hidden="true"></i> HelloAsso]({{ site.data.extlinks.helloasso-ccl }}){: .btn .btn--inverse}
- En nous rencontrant, par exemple au local de l'hexagone ou devant la
  cafétéria [Contacts](/contacts){: .btn .btn--inverse}

## L'adhésion nous permet de nous aider
- financièrement : 1+1+1+...=&infin;,
- à quantifier globalement nos les participants à nos activités et nos
  soutiens,
- à avoir du poids vis à vis des admimnistrations pour débloquer de nouvelles
  initiatives.

Mis à part nos événements publics, les participants à nos activités,
officiellement, doivent être adhérent (i.e. membre) à l'association du Centre
Culturel de Luminy.
{: .notice}


{% include_relative devenir-benevole.md %}
