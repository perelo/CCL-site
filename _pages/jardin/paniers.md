---
title: Les paniers bio !
header:
  overlay_image: assets/images/banniere-bulles.png
---

Tous les jeudis, les paniers marseillais proposent des produits bio en direct de chez les producteurs ; légumes, fruits, œufs, pain, jus, vin, huile, olives, fruits secs, vinaigre, viande, fromages, ...

# Distribution paniers bio
[Cagette.net]({{ site.data.extlinks.cagette }})
